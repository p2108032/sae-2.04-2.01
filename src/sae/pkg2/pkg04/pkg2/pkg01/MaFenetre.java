/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sae.pkg2.pkg04.pkg2.pkg01;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author p2108032
 */
public class MaFenetre extends JFrame implements ActionListener {

    private JTextField txt;
    private JButton bouton;
    private JLabel label;
    private JLabel labelB;
    private JLabel labelNb;
    private JComboBox<String> combo;
    private static final long serialVersionUID = 1L;

    public MaFenetre() {
        super("test");
        this.setTitle("Convertisseur");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initialisation();
        //bouton.addActionListener(this);
        //txt.addFocusListener(this);
    }

    private void initialisation() {
//        txt=new JTextField("Nombre à saisir");
//        txt.setColumns(10);
//        bouton= new JButton("CONVERT");
//        label= new JLabel("txt par defaut");
//        labelNb= new JLabel("Nombre (en base 10):");
//        labelB= new JLabel("Base:");
//        label.setBorder(new BevelBorder(1));
//        labelNb.setBorder(new BevelBorder(1));
//        labelB.setBorder(new BevelBorder(1));
//        String t[]={"2","3","8","16"};
//        combo = new JComboBox(t);
//        
//        JPanel pano= new JPanel();
//        pano.setLayout(new GridBagLayout());
//        GridBagConstraints cont= new GridBagConstraints();
//        
//        cont.fill=GridBagConstraints.BOTH;
//        cont.gridx=0;
//        cont.gridy=0;
//        pano.add(labelNb, cont);
//        
//        cont.gridx=1;
//        cont.gridy=0;
//        pano.add(labelB, cont);
//        
//        cont.gridx=0;
//        cont.gridy=1;
//        pano.add(txt, cont);
//        
//        cont.gridx=1;
//        cont.gridy=1;
//        pano.add(combo, cont);
//        
//        cont.gridx=0;
//        cont.gridy=2;
//        cont.gridwidth=2;
//        pano.add(bouton, cont);
//        
//        cont.gridx=0;
//        cont.gridy=3;
//        cont.gridwidth=2;
//        pano.add(label, cont);
//        
//        this.setContentPane(pano);
//        this.pack();
        //Creates a sample dataset

        JPanel pano = new JPanel();
        pano.setLayout(new GridBagLayout());
        GridBagConstraints cont = new GridBagConstraints();

        DefaultPieDataset dataSet = new DefaultPieDataset();
        dataSet.setValue("Chrome", 29);
        dataSet.setValue("InternetExplorer", 36);
        dataSet.setValue("Firefox", 35);

        // based on the dataset we create the chart
        JFreeChart pieChart = ChartFactory.createPieChart3D("youhou", dataSet, true, true, false);
        PiePlot plot = (PiePlot) pieChart.getPlot();
        plot.setStartAngle(290);
        plot.setForegroundAlpha(0.5f);

        ChartPanel chartPanel = new ChartPanel(pieChart);

        cont.gridx = 0;
        cont.gridy = 0;
        cont.gridwidth = 1;
        pano.add(chartPanel, cont);

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        dataset.addValue(120000.0, "Produit 1", "2000");
        dataset.addValue(550000.0, "Produit 1", "2001");
        dataset.addValue(180000.0, "Produit 1", "2002");
        dataset.addValue(270000.0, "Produit 2", "2000");
        dataset.addValue(600000.0, "Produit 2", "2001");
        dataset.addValue(230000.0, "Produit 2", "2002");
        dataset.addValue(90000.0, "Produit 3", "2000");
        dataset.addValue(450000.0, "Produit 3", "2001");
        dataset.addValue(170000.0, "Produit 3", "2002");

        JFreeChart barChart = ChartFactory.createBarChart("Evolution des ventes", "", "Unité vendue", dataset, PlotOrientation.VERTICAL, true, true, false);
        ChartPanel cPanel = new ChartPanel(barChart);
        cont.gridx = 0;
        cont.gridy = 1;
        cont.gridwidth = 1;
        pano.add(cPanel, cont);
        pano.add(cPanel);

        // settind default size
        // add to contentPane
        setContentPane(pano);

        this.pack();
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}